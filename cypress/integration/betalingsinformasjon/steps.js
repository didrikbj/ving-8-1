import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('7');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('9');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, ()=>{
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, ()=>{
    cy.get('#fullName').type('ole hansen').blur();
    cy.get('#address').type('hansen veien 5c').blur();
    cy.get('#postCode').type('7777').blur();
    cy.get('#city').type('trondheim').blur();
    cy.get('#creditCardNo').type('1256123456780954').blur();
});

And(/^trykker på Fullfør kjøp$/, ()=>{
    cy.get('input[type=submit]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, ()=>{
    cy.get(".confirmation").should("exist");
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('8');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

});

And(/^trykket på Gå til betaling$/, ()=>{
    cy.get('input[type=submit]').click();
});

And(/^jeg legger inn ugyldige verdier i feltene$/, ()=>{

    cy.get('#fullName').clear().blur();
    cy.get('#address').clear().blur();
    cy.get('#postCode').clear().blur();
    cy.get('#city').clear().blur();
    cy.get('#creditCardNo').clear().blur();

    
});

And(/^skal jeg få feilmeldinger for disse$/, ()=>{
    cy.get('#fullNameError').should('have.text', 'Feltet må ha en verdi')
    cy.get('#addressError').should('have.text', 'Feltet må ha en verdi')
    cy.get('#postCodeError').should('have.text', 'Feltet må ha en verdi')
    cy.get('#cityError').should('have.text', 'Feltet må ha en verdi')
    cy.get('#creditCardNoError').should('have.text', "Kredittkortnummeret må bestå av 16 siffer")
});